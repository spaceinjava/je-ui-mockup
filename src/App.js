import React, { Component } from 'react';
import Layout from './components/Layout/Layout';
import { BrowserRouter } from 'react-router-dom';
import JewelEasy from './containers/JewelEasy/JewelEasy';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Layout>
          <JewelEasy />
        </Layout>
      </BrowserRouter>
    );
  }
}

export default App;
