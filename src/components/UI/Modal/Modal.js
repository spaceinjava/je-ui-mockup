import React from 'react';
import classes from './Modal.module.css';
import Aux from '../../../hoc/auxillary';
import Backdrop from '../Backdrop/Backdrop';

const modal = props => {
  const { show } = props;
  return (
    <Aux>
      <Backdrop show={show} />
      <div
        className={classes.Modal}
        style={{
          transform: show ? 'translateY(0)' : 'translateY(-100vh)',
          opacity: show ? '1' : '0'
        }}
      >
        {props.children}
      </div>
    </Aux>
  );
};

export default modal;
