import React from 'react';

import jewelEasy from '../../assets/images/logo.png';
import classes from './Logo.module.css';

const logo = props => <img src={jewelEasy} alt="jewelEasy" />;

export default logo;
