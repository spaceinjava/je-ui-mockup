import React from 'react';
import Aux from '../../hoc/auxillary';
import svgIcons from '../../assets/images/images';

const dashboard = props => {
  return (
    <Aux>
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-6"></div>
        </div>
        <div className="row">
          <div className="col-md-12">
            <div className="panel panel-default hero-panel">
              <div className="panel-heading">
                <h1 className="panel-title">Dashboard</h1>
              </div>

              <div className="panel-body">
                <div className="row">
                  <div className="col-md-7">
                    <div className="panel panel-default">
                      <div className="panel-heading">
                        <h3 className="panel-title">All Transactions</h3>
                        <div className="panel-options">
                          <a href="#" className="btn btn-primary btn-sm">
                            View All
                          </a>
                        </div>
                      </div>
                      <div className="panel-body">
                        <img
                          src={svgIcons.areachart}
                          alt="chart"
                          style={{ width: '100%' }}
                        />
                      </div>
                    </div>
                    <div className="panel panel-default">
                      <div className="panel-heading">
                        <h3 className="panel-title">Today Stock Details</h3>
                        <div className="panel-options">
                          <a href="#" className="btn btn-primary  btn-sm ">
                            View All
                          </a>
                        </div>
                      </div>
                      <div className="panel-body">
                        <div className="row">
                          <div className="col-md-6  text-center">
                            <a href="#" className="card barcoded">
                              <h1 className="text-dark no-margin">
                                <span className="text-small">₹</span>
                                52,20,50
                              </h1>
                              <h4 className="no-margin">Bar Coded</h4>
                            </a>
                          </div>
                          <div className="col-md-6 text-center">
                            <a href="#" className="card stockPurchase">
                              <h1 className="text-dark no-margin">
                                <span className="text-small">₹</span> 80,50,500
                              </h1>
                              <h4 className="no-margin">Stock Purchases</h4>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="panel panel-default">
                      <div className="panel-heading">
                        <h3 className="panel-title">Customer Due’s</h3>
                      </div>
                      <div className="panel-body max-height-h1">
                        <div className="amountHilight">
                          <h1 className="text-dark no-margin">
                            <span className="text-small">₹</span> 25,15,320
                          </h1>
                          <h4 className="no-margin">Customer Due’s</h4>
                        </div>
                        <div className="row">
                          <div className="text-center">
                            <img src={svgIcons.barCHart} />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-5">
                    <div className="panel panel-default">
                      <div className="panel-heading">
                        <h3 className="panel-title">Today Transactions</h3>
                        <div className="panel-options">
                          <a href="#" className="btn btn-primary  btn-sm">
                            View All
                          </a>
                        </div>
                      </div>
                      <div className="panel-body">
                        <div className="amountHilight">
                          <h1 className="text-dark no-margin">
                            <span className="text-small">₹</span> 75,50,230
                          </h1>
                          <h4 className="no-margin">Today Transactions</h4>
                        </div>

                        <div className="row">
                          <div className="col-md-12">
                            <div className="soldCardWrapper">
                              <a href="#">
                                <div className="soldCard">
                                  <div className="soldGraph">
                                    <img src={svgIcons.pieGold} alt="Chart" />
                                  </div>
                                  <div className="soldInfo">
                                    <span className="soldTitle">Gold</span>
                                    <span className="soldAmount">
                                      10,20,000<sup>₹</sup>
                                    </span>
                                    <span className="soldUnit">
                                      1,492.5 <sub>grm</sub>
                                    </span>
                                  </div>
                                </div>
                              </a>
                            </div>
                            <div className="soldCardWrapper">
                              <a href="#">
                                <div className="soldCard">
                                  <div className="soldGraph">
                                    <img src={svgIcons.pieSilver} alt="Chart" />
                                  </div>
                                  <div className="soldInfo">
                                    <span className="soldTitle">Silver</span>
                                    <span className="soldAmount">
                                      5,20,000<sup>₹</sup>
                                    </span>
                                    <span className="soldUnit">
                                      1,492.5 <sub>grm</sub>
                                    </span>
                                  </div>
                                </div>
                              </a>
                            </div>
                            <div className="soldCardWrapper">
                              <a href="#">
                                <div className="soldCard">
                                  <div className="soldGraph">
                                    <img
                                      src={svgIcons.pieDiamonds}
                                      alt="Chart"
                                    />
                                  </div>
                                  <div className="soldInfo">
                                    <span className="soldTitle">Diamond</span>
                                    <span className="soldAmount">
                                      9,20,000<sup>₹</sup>
                                    </span>
                                    <span className="soldUnit">
                                      1,492.5 <sub>grm</sub>
                                    </span>
                                  </div>
                                </div>
                              </a>
                            </div>
                            <div className="soldCardWrapper">
                              <a href="#">
                                <div className="soldCard">
                                  <div className="soldGraph">
                                    <img
                                      src={svgIcons.piePlatinum}
                                      alt="Chart"
                                    />
                                  </div>
                                  <div className="soldInfo">
                                    <span className="soldTitle">Platinum</span>
                                    <span className="soldAmount">
                                      6,20,000<sup>₹</sup>
                                    </span>
                                    <span className="soldUnit">
                                      1,492.5 <sub>grm</sub>
                                    </span>
                                  </div>
                                </div>
                              </a>
                            </div>
                            <div className="soldCardWrapper">
                              <a href="#">
                                <div className="soldCard">
                                  <div className="soldGraph">
                                    <img
                                      src={svgIcons.pieGemStones}
                                      alt="Chart"
                                    />
                                  </div>
                                  <div className="soldInfo">
                                    <span className="soldTitle">
                                      Gem Stones
                                    </span>
                                    <span className="soldAmount">
                                      9,20,000<sup>₹</sup>
                                    </span>
                                    <span className="soldUnit">
                                      1,492.5 <sub>grm</sub>
                                    </span>
                                  </div>
                                </div>
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="panel panel-default">
                      <div className="panel-heading">
                        <h3 className="panel-title">Orders Summary</h3>
                        <div className="panel-options">
                          <form className="form-inline">
                            <select className="form-control input-sm">
                              <option>Today</option>
                              <option>Yesterday</option>
                            </select>
                          </form>
                        </div>
                      </div>
                      <div className="panel-body">
                        <div className="row">
                          <div className="col-md-6">
                            <a href="#" className="summary-card registred">
                              <img
                                src={svgIcons.registered}
                                alt="Registered Orders"
                              />
                              <h5>Registered Orders</h5>
                              <h2>520</h2>
                            </a>
                          </div>
                          <div className="col-md-6">
                            <a
                              href="#"
                              className="summary-card readytoDelivery"
                            >
                              <img
                                src={svgIcons.readytoDelever}
                                alt="Ready For Delivery "
                              />
                              <h5>Ready For Delivery</h5>
                              <h2>50</h2>
                            </a>
                          </div>
                          <div className="col-md-6">
                            <a href="#" className="summary-card delevered">
                              <img
                                src={svgIcons.delevered}
                                alt="Delivered Orders"
                              />
                              <h5>Delivered Orders</h5>
                              <h2>40</h2>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="panel panel-default">
                      <div className="panel-heading">
                        <h3 className="panel-title">Reminders</h3>
                      </div>
                      <div className="panel-body max-height-h1">
                        <div className="row">
                          <div className="col-md-6">dfds</div>
                        </div>
                      </div>
                    </div>
                    <div className="panel panel-default">
                      <div className="panel-heading">
                        <h3 className="panel-title">Wishes</h3>
                      </div>
                      <div className="panel-body max-height-h1">
                        <div className="row">
                          <div className="col-md-6">
                            <a href="#" className="summary-card birthdays">
                              <img
                                src={svgIcons.readytoDelever}
                                alt="Ready For Delivery "
                              />
                              <h5>Birthdays</h5>
                              <h2>50</h2>
                            </a>
                          </div>
                          <div className="col-md-6">
                            <a href="#" className="summary-card occations">
                              <img
                                src={svgIcons.readytoDelever}
                                alt="Ready For Delivery "
                              />
                              <h5>Occations</h5>
                              <h2>50</h2>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Aux>
  );
};

export default dashboard;
