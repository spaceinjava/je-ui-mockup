import React from 'react';
import { Link, NavLink } from 'react-router-dom';

import classes from './SideDrawer.module.css';
import Backdrop from '../../UI/Backdrop/Backdrop';
import Aux from '../../../hoc/auxillary';
import svgIcons from '../../../assets/images/images';

const sideDrawer = props => {
  let attachedClasses = [classes.SideDrawer, classes.Close];
  if (props.open) {
    attachedClasses = [classes.SideDrawer, classes.Open];
  }
  return (
    <Aux>
      <Backdrop show={props.open} clicked={props.closed} />
      <div className="left-nav">
        <ul className="iconTabs">
          <li>
            <NavLink to={{ pathname: '/dashboard' }}>
              <span className="tabIcon">
                <img
                  src={svgIcons.dashboardIcon}
                  width="96"
                  height="96"
                  alt="Dashboard"
                />
              </span>
              <span className="tabTitle">Dashboard</span>
            </NavLink>
          </li>

          <li>
            <NavLink to={{ pathname: '/admin' }}>
              <span className="tabIcon">
                <img
                  src={svgIcons.adminIcon}
                  width="96"
                  height="96"
                  alt="Admin"
                />
              </span>
              <span className="tabTitle">Admin</span>
            </NavLink>
          </li>
          <li>
            <NavLink to={{ pathname: '/creations' }}>
              <span className="tabIcon">
                <img
                  src={svgIcons.creations}
                  width="96"
                  height="96"
                  alt="Creations"
                />
              </span>
              <span className="tabTitle">Creations</span>
            </NavLink>
          </li>

          <li>
            <NavLink to={{ pathname: '/transactions' }}>
              <span className="tabIcon">
                <img
                  src={svgIcons.transactions}
                  width="96"
                  height="96"
                  alt="Transactions"
                />
              </span>
              <span className="tabTitle">Transactions</span>
            </NavLink>
          </li>

          <li>
            <NavLink to={{ pathname: '/reports' }}>
              <span className="tabIcon">
                <img
                  src={svgIcons.reports}
                  width="96"
                  height="96"
                  alt="Reports"
                />
              </span>
              <span className="tabTitle">Reports</span>
            </NavLink>
          </li>

          <li>
            <NavLink to={{ pathname: '/utilities' }}>
              <span className="tabIcon">
                <img
                  src={svgIcons.utilities}
                  width="96"
                  height="96"
                  alt="Utilities"
                />
              </span>
              <span className="tabTitle">Utilities</span>
            </NavLink>
          </li>
        </ul>
      </div>
    </Aux>
  );
};

export default sideDrawer;
