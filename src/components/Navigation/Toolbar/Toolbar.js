import React from 'react';

import classes from './Toolbar.module.css';
import Logo from '../../Logo/Logo';
import NavigationItems from '../NavigationItems/NavigationItems';
import DrawerToggle from '../SideDrawer/DrawerToggle/DrawerToggle';
import userImg from '../../../../src/assets/images/user.jpg';

const toolbar = props => (
  // <header className={classes.Toolbar}>
  //   <DrawerToggle clicked={props.drawerToggleClicked} />
  //   {/* <div className={classes.Logo}>
  //     <Logo />
  //   </div>
  //   <nav className={classes.DesktopOnly}>
  //     <NavigationItems />
  //   </nav> */}
  // </header>
  <div className="mainHeader clearfix">
    <div className="headerLogo">
      <a href="index.html">
        <Logo />
      </a>
    </div>
    <div className="header-right">
      <ul className="headerCntnt">
        <li className="language">
          <div className="dropdown">
            <button
              className="btn btn-link dropdown-toggle"
              type="button"
              id="dropdownMenu1"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="true"
            >
              <i className="zmdi zmdi-translate"></i>
              <span className="hidden-xs">Language</span>
              <span className="caret"></span>
            </button>
            <ul
              className="dropdown-menu dropdown-right"
              aria-labelledby="dropdownMenu1"
            >
              <li>
                <a href="#">English</a>
              </li>
              <li>
                <a href="#">Hindhi</a>
              </li>
            </ul>
          </div>
        </li>

        <li className="date hidden-xs">
          <span className="hidden-xs">Sunday,</span> Jan 05, 2019
        </li>

        <li className="userInfo">
          <div className="dropdown">
            <button
              className="btn btn-link dropdown-toggle"
              type="button"
              id="dropdownMenu1"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="true"
            >
              <div className="userImg">
                <img src={userImg} alt="Img" />
              </div>
              <span className="hidden-xs">Sita Ram Narayan</span>
              <span className="caret"></span>
            </button>
            <ul
              className="dropdown-menu dropdown-right"
              aria-labelledby="dropdownMenu1"
            >
              <li>
                <a href="#">Settings</a>
              </li>
              <li>
                <a href="#">Logout</a>
              </li>
            </ul>
          </div>
        </li>
      </ul>
    </div>
  </div>
);

export default toolbar;
