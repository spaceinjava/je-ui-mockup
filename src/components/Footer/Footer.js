import React from 'react';

const footer = props => {
  return (
    <div className="main-footer">
      2020 © JewelEasy, Powered by <a href="#">Spacein Technologies</a>
    </div>
  );
};

export default footer;
