import React, { Component } from 'react';
import Aux from '../../hoc/auxillary';
import '../../assets/css/datatables.css';

const $ = require('jquery');
$.DataTable = require('datatables.net');

class Reports extends Component {
  componentDidMount() {
    $('#alltransactions').DataTable({
      columnDefs: [
        {
          targets: [0],
          orderData: [0, 1],
        },
        {
          targets: [1],
          orderData: [1, 0],
        },
        {
          targets: [4],
          orderData: [4, 0],
        },
      ],
      language: {
        paginate: {
          previous: '',
          next: '',
        },
      },
    });
  }
  componentWillUnmount() {
    $('#alltransactions').DataTable().destroy(true);
  }
  render() {
    return (
      <Aux>
        <div className="container-fluid">
          <div className="row">
            <div className="innerContainer">
              <div className="innerLeftNav">
                <div className="left-nav-holder">
                  <ul className="left-subnav" id="sidebar">
                    <li className="hasAccordian hide">
                      {' '}
                      <a
                        className="collapsed"
                        role="button"
                        data-toggle="collapse"
                        href="#PreQualify"
                        aria-expanded="true"
                        aria-controls="PreQualify"
                      >
                        {' '}
                        Employees
                      </a>
                      <ul className="collapse" id="PreQualify">
                        <li>
                          <a href="#need_for_insurance">
                            <span>Need For Insurance</span>
                          </a>
                        </li>
                      </ul>
                    </li>
                    <li>
                      {' '}
                      <a href="#" className="active">
                        {' '}
                        Sales Report
                      </a>{' '}
                    </li>
                    <li>
                      {' '}
                      <a href="#"> Stock Reports</a>{' '}
                    </li>
                  </ul>
                </div>
              </div>
              <div className="innerRightCntnt">
                <div className="panel panel-default hero-panel">
                  <div className="panel-heading">
                    <h1 className="panel-title">Stock Reports </h1>
                    <div
                      className="modal fade"
                      id="addNew"
                      tabIndex="-1"
                      role="dialog"
                      aria-labelledby="myModalLabel"
                    >
                      <div className="modal-dialog modal-lg" role="document">
                        <div className="modal-content">
                          <div className="modal-header">
                            <button
                              type="button"
                              className="close"
                              data-dismiss="modal"
                              aria-label="Close"
                            >
                              <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 className="modal-title" id="myModalLabel">
                              Modal title
                            </h4>
                          </div>
                          <div className="modal-body">
                            <div className="panel panel-default">
                              <div className="panel-heading">
                                <h3 className="panel-title">
                                  Personal Details{' '}
                                </h3>
                              </div>
                              <div className="panel-body">
                                <div className="col-md-4">
                                  <div className="form-group">
                                    <label for="">First Name</label>
                                    <input
                                      type="text"
                                      className="form-control"
                                      placeholder="Vamsi"
                                    />
                                  </div>
                                </div>
                                <div className="col-md-4">
                                  <div className="form-group">
                                    <label for="">Middle Name</label>
                                    <input
                                      type="text"
                                      className="form-control"
                                      placeholder="K"
                                    />
                                  </div>
                                </div>
                                <div className="col-md-4">
                                  <div className="form-group">
                                    <label for="">Last Name</label>
                                    <input
                                      type="text"
                                      className="form-control"
                                      placeholder="Krishna"
                                    />
                                  </div>
                                </div>
                                <div className="col-md-4">
                                  <div className="form-group">
                                    <label className="control-label">
                                      Gender
                                    </label>
                                    <div className="radio radio-inline radio-primary">
                                      <input
                                        type="radio"
                                        name="Gender"
                                        id="Gender_M"
                                      />
                                      <label for="Gender_M">Male</label>
                                    </div>
                                    <div className="radio radio-inline radio-primary">
                                      <input
                                        type="radio"
                                        name="Gender"
                                        id="Gender_F"
                                      />
                                      <label for="Gender_F">Female</label>
                                    </div>
                                  </div>
                                </div>
                                <div className="col-md-4">
                                  <div className="form-group">
                                    <label className="control-label">
                                      Checkbox Default
                                    </label>
                                    <div className="checkbox checkbox-inline checkbox-primary">
                                      <input type="checkbox" id="Checkbox1" />
                                      <label for="Checkbox1">Checkbox 1 </label>
                                    </div>

                                    <div className="checkbox checkbox-inline checkbox-primary">
                                      <input type="checkbox" id="Checkbox2" />
                                      <label for="Checkbox2">Checkbox 2 </label>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <div className="panel panel-default">
                              <div className="panel-heading">
                                <h3 className="panel-title">
                                  Personal Details{' '}
                                </h3>
                              </div>
                              <div className="panel-body">
                                <div className="col-md-4">
                                  <div className="form-group">
                                    <label for="">First Name</label>
                                    <input
                                      type="text"
                                      className="form-control"
                                      placeholder="Vamsi"
                                    />
                                  </div>
                                </div>
                                <div className="col-md-4">
                                  <div className="form-group">
                                    <label for="">Middle Name</label>
                                    <input
                                      type="text"
                                      className="form-control"
                                      placeholder="K"
                                    />
                                  </div>
                                </div>
                                <div className="col-md-4">
                                  <div className="form-group">
                                    <label for="">Last Name</label>
                                    <input
                                      type="text"
                                      className="form-control"
                                      placeholder="Krishna"
                                    />
                                  </div>
                                </div>
                                <div className="col-md-4">
                                  <div className="form-group">
                                    <label className="control-label">
                                      Gender
                                    </label>
                                    <div className="radio radio-inline radio-primary">
                                      <input
                                        type="radio"
                                        name="Gender"
                                        id="Gender_M"
                                      />
                                      <label for="Gender_M">Male</label>
                                    </div>
                                    <div className="radio radio-inline radio-primary">
                                      <input
                                        type="radio"
                                        name="Gender"
                                        id="Gender_F"
                                      />
                                      <label for="Gender_F">Female</label>
                                    </div>
                                  </div>
                                </div>
                                <div className="col-md-4">
                                  <div className="form-group">
                                    <label className="control-label">
                                      Checkbox Default
                                    </label>
                                    <div className="checkbox checkbox-inline checkbox-primary">
                                      <input type="checkbox" id="Checkbox1" />
                                      <label for="Checkbox1">Checkbox 1 </label>
                                    </div>

                                    <div className="checkbox checkbox-inline checkbox-primary">
                                      <input type="checkbox" id="Checkbox2" />
                                      <label for="Checkbox2">Checkbox 2 </label>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <div className="panel panel-default">
                              <div className="panel-heading">
                                <h3 className="panel-title">
                                  Personal Details{' '}
                                </h3>
                              </div>
                              <div className="panel-body">
                                <div className="col-md-4">
                                  <div className="form-group">
                                    <label for="">First Name</label>
                                    <input
                                      type="text"
                                      className="form-control"
                                      placeholder="Vamsi"
                                    />
                                  </div>
                                </div>
                                <div className="col-md-4">
                                  <div className="form-group">
                                    <label for="">Middle Name</label>
                                    <input
                                      type="text"
                                      className="form-control"
                                      placeholder="K"
                                    />
                                  </div>
                                </div>
                                <div className="col-md-4">
                                  <div className="form-group">
                                    <label for="">Last Name</label>
                                    <input
                                      type="text"
                                      className="form-control"
                                      placeholder="Krishna"
                                    />
                                  </div>
                                </div>
                                <div className="col-md-4">
                                  <div className="form-group">
                                    <label className="control-label">
                                      Gender
                                    </label>
                                    <div className="radio radio-inline radio-primary">
                                      <input
                                        type="radio"
                                        name="Gender"
                                        id="Gender_M"
                                      />
                                      <label for="Gender_M">Male</label>
                                    </div>
                                    <div className="radio radio-inline radio-primary">
                                      <input
                                        type="radio"
                                        name="Gender"
                                        id="Gender_F"
                                      />
                                      <label for="Gender_F">Female</label>
                                    </div>
                                  </div>
                                </div>
                                <div className="col-md-4">
                                  <div className="form-group">
                                    <label className="control-label">
                                      Checkbox Default
                                    </label>
                                    <div className="checkbox checkbox-inline checkbox-primary">
                                      <input type="checkbox" id="Checkbox1" />
                                      <label for="Checkbox1">Checkbox 1 </label>
                                    </div>

                                    <div className="checkbox checkbox-inline checkbox-primary">
                                      <input type="checkbox" id="Checkbox2" />
                                      <label for="Checkbox2">Checkbox 2 </label>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="modal-footer">
                            <button
                              type="button"
                              className="btn btn-default btn-lg"
                              data-dismiss="modal"
                            >
                              Cancel
                            </button>
                            <button
                              type="button"
                              className="btn btn-primary  btn-lg"
                            >
                              Save
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="panel-body">
                    <div className="panel panel-default">
                      <div className="panel-heading hidden">
                        <h3 className="panel-title">Contact Details </h3>
                      </div>
                      <div className="panel-body">
                        <p>
                          {' '}
                          <a
                            href="#"
                            className="btn btn-success"
                            data-toggle="modal"
                            data-target="#addNew"
                          >
                            <i className="zmdi zmdi-playlist-plus"></i> Create
                            New
                          </a>{' '}
                          <a
                            href="#"
                            className="btn btn-default"
                            data-toggle="modal"
                            data-target="#addNew"
                            disabled
                          >
                            <i className="zmdi zmdi-delete"></i> Delete
                          </a>
                        </p>
                        <div className="table-responsive">
                          <table id="alltransactions" className="display">
                            <thead>
                              <tr>
                                <th></th>
                                <th width="15%">Item ID.</th>
                                <th width="26%">Item Name</th>
                                <th width="18%">Amount(â‚¹)</th>
                                <th width="24%">Party Name</th>
                                <th width="17%">Ph. Number</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>
                                  <div className="checkbox checkbox-primary table-checkbox">
                                    <input type="checkbox" id="Checkbox1" />
                                    <label for="Checkbox1"></label>
                                  </div>
                                </td>
                                <td>
                                  <a href="#">880416</a>
                                </td>
                                <td>Tanmaniya</td>
                                <td>2,10,200</td>
                                <td>Muhammad</td>
                                <td>15465 12451</td>
                              </tr>
                              <tr>
                                <td>
                                  <div className="checkbox checkbox-primary table-checkbox">
                                    <input type="checkbox" id="Checkbox2" />
                                    <label for="Checkbox2"></label>
                                  </div>
                                </td>
                                <td>
                                  <a href="#">599307</a>
                                </td>
                                <td>Bangles</td>
                                <td>1,72,086</td>
                                <td>Arjun</td>
                                <td>99122 45111</td>
                              </tr>
                              <tr>
                                <td>
                                  <div className="checkbox checkbox-primary table-checkbox">
                                    <input type="checkbox" id="Checkbox3" />
                                    <label for="Checkbox3"></label>
                                  </div>
                                </td>
                                <td>
                                  <a href="#">880416</a>
                                </td>
                                <td>Necklace</td>
                                <td>1,50,102</td>
                                <td>Madhavaditya</td>
                                <td>83254 65123</td>
                              </tr>
                              <tr>
                                <td>
                                  <div className="checkbox checkbox-primary table-checkbox">
                                    <input type="checkbox" id="Checkbox4" />
                                    <label for="Checkbox4"></label>
                                  </div>
                                </td>
                                <td>
                                  <a href="#">880416</a>
                                </td>
                                <td>Pandent</td>
                                <td>5,16,559</td>
                                <td>Reyansh</td>
                                <td>44156 84575</td>
                              </tr>
                              <tr>
                                <td>
                                  <div className="checkbox checkbox-primary table-checkbox">
                                    <input type="checkbox" id="Checkbox5" />
                                    <label for="Checkbox5"></label>
                                  </div>
                                </td>
                                <td>
                                  <a href="#">880416</a>
                                </td>
                                <td>Pandent</td>
                                <td>3,12,150</td>
                                <td>Goutham</td>
                                <td>96455 45454</td>
                              </tr>
                              <tr>
                                <td>
                                  <div className="checkbox checkbox-primary table-checkbox">
                                    <input type="checkbox" id="Checkbox6" />
                                    <label for="Checkbox6"></label>
                                  </div>
                                </td>
                                <td>
                                  <a href="#">880416</a>
                                </td>
                                <td>Pandent</td>
                                <td>16,560</td>
                                <td>Shiva Prasad</td>
                                <td>78454 45784</td>
                              </tr>
                              <tr>
                                <td>
                                  <div className="checkbox checkbox-primary table-checkbox">
                                    <input type="checkbox" id="Checkbox7" />
                                    <label for="Checkbox7"></label>
                                  </div>
                                </td>
                                <td>
                                  <a href="#">880416</a>
                                </td>
                                <td>Tanmaniya</td>
                                <td>2,10,200</td>
                                <td>Muhammad</td>
                                <td>15465 12451</td>
                              </tr>
                              <tr>
                                <td>
                                  <div className="checkbox checkbox-primary table-checkbox">
                                    <input type="checkbox" id="Checkbox8" />
                                    <label for="Checkbox8"></label>
                                  </div>
                                </td>
                                <td>
                                  <a href="#">599307</a>
                                </td>
                                <td>Bangles</td>
                                <td>1,72,086</td>
                                <td>Arjun</td>
                                <td>99122 45111</td>
                              </tr>
                              <tr>
                                <td>
                                  <div className="checkbox checkbox-primary table-checkbox">
                                    <input type="checkbox" id="Checkbox9" />
                                    <label for="Checkbox9"></label>
                                  </div>
                                </td>
                                <td>
                                  <a href="#">880416</a>
                                </td>
                                <td>Necklace</td>
                                <td>1,50,102</td>
                                <td>Madhavaditya</td>
                                <td>83254 65123</td>
                              </tr>
                              <tr>
                                <td>
                                  <div className="checkbox checkbox-primary table-checkbox">
                                    <input type="checkbox" id="Checkbox10" />
                                    <label for="Checkbox10"></label>
                                  </div>
                                </td>
                                <td>
                                  <a href="#">880416</a>
                                </td>
                                <td>Pandent</td>
                                <td>5,16,559</td>
                                <td>Reyansh</td>
                                <td>44156 84575</td>
                              </tr>
                              <tr>
                                <td>
                                  <div className="checkbox checkbox-primary table-checkbox">
                                    <input type="checkbox" id="Checkbox11" />
                                    <label for="Checkbox11"></label>
                                  </div>
                                </td>
                                <td>
                                  <a href="#">880416</a>
                                </td>
                                <td>Pandent</td>
                                <td>3,12,150</td>
                                <td>Goutham</td>
                                <td>96455 45454</td>
                              </tr>
                              <tr>
                                <td>
                                  <div className="checkbox checkbox-primary table-checkbox">
                                    <input type="checkbox" id="Checkbox12" />
                                    <label for="Checkbox12"></label>
                                  </div>
                                </td>
                                <td>
                                  <a href="#">880416</a>
                                </td>
                                <td>Pandent</td>
                                <td>16,560</td>
                                <td>Shiva Prasad</td>
                                <td>78454 45784</td>
                              </tr>
                              <tr>
                                <td>
                                  <div className="checkbox checkbox-primary table-checkbox">
                                    <input type="checkbox" id="Checkbox13" />
                                    <label for="Checkbox13"></label>
                                  </div>
                                </td>
                                <td>
                                  <a href="#">880416</a>
                                </td>
                                <td>Tanmaniya</td>
                                <td>2,10,200</td>
                                <td>Muhammad</td>
                                <td>15465 12451</td>
                              </tr>
                              <tr>
                                <td>
                                  <div className="checkbox checkbox-primary table-checkbox">
                                    <input type="checkbox" id="Checkbox14" />
                                    <label for="Checkbox14"></label>
                                  </div>
                                </td>
                                <td>
                                  <a href="#">599307</a>
                                </td>
                                <td>Bangles</td>
                                <td>1,72,086</td>
                                <td>Arjun</td>
                                <td>99122 45111</td>
                              </tr>
                              <tr>
                                <td>
                                  <div className="checkbox checkbox-primary table-checkbox">
                                    <input type="checkbox" id="Checkbox15" />
                                    <label for="Checkbox15"></label>
                                  </div>
                                </td>
                                <td>
                                  <a href="#">880416</a>
                                </td>
                                <td>Necklace</td>
                                <td>1,50,102</td>
                                <td>Madhavaditya</td>
                                <td>83254 65123</td>
                              </tr>
                              <tr>
                                <td>
                                  <div className="checkbox checkbox-primary table-checkbox">
                                    <input type="checkbox" id="Checkbox16" />
                                    <label for="Checkbox16"></label>
                                  </div>
                                </td>
                                <td>
                                  <a href="#">880416</a>
                                </td>
                                <td>Pandent</td>
                                <td>5,16,559</td>
                                <td>Reyansh</td>
                                <td>44156 84575</td>
                              </tr>
                              <tr>
                                <td>
                                  <div className="checkbox checkbox-primary table-checkbox">
                                    <input type="checkbox" id="Checkbox17" />
                                    <label for="Checkbox17"></label>
                                  </div>
                                </td>
                                <td>
                                  <a href="#">880416</a>
                                </td>
                                <td>Pandent</td>
                                <td>3,12,150</td>
                                <td>Goutham</td>
                                <td>96455 45454</td>
                              </tr>
                              <tr>
                                <td>
                                  <div className="checkbox checkbox-primary table-checkbox">
                                    <input type="checkbox" id="Checkbox18" />
                                    <label for="Checkbox18"></label>
                                  </div>
                                </td>
                                <td>
                                  <a href="#">880416</a>
                                </td>
                                <td>Pandent</td>
                                <td>16,560</td>
                                <td>Shiva Prasad</td>
                                <td>78454 45784</td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Aux>
    );
  }
}

export default Reports;
