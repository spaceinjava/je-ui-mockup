import React from 'react';
import { Route } from 'react-router-dom';

import Aux from '../../hoc/auxillary';
import Admin from '../Admin/Admin';
import Dashboard from '../Dashboard/Dashboard';
import Footer from '../Footer/Footer';
import Creations from '../Creations/Creations';
import Transactions from '../Transactions/Transactions';
import Reports from '../Reports/Reports';
import Uitilies from '../Uilities/Uilities';

const mainContainer = props => {
  return (
    <Aux>
      <div className="right-content">
        <Route path="/" exact component={Dashboard} />
        <Route path="/dashboard" exact component={Dashboard} />
        <Route path="/admin" exact component={Admin} />
        <Route path="/creations" exact component={Creations} />
        <Route path="/transactions" exact component={Transactions} />
        <Route path="/reports" exact component={Reports} />
        <Route path="/utilities" exact component={Uitilies} />
        <Footer />
      </div>
    </Aux>
  );
};

export default mainContainer;
