import dashboardIcon from './dashboard.png';
import adminIcon from './admin.png';
import creations from './creations.png';
import transactions from './transactions.png';
import reports from './reports.png';
import utilities from './utilities.png';
import areachart from './areachart.svg';
import barCHart from './barCHart.svg';
import pieGold from './pie_gold.svg';
import pieSilver from './pie_silver.svg';
import pieDiamonds from './pie_diamonds.svg';
import piePlatinum from './pie_platinum.svg';
import pieGemStones from './pie_gemStones.svg';
import registered from './registered.svg';
import readytoDelever from './readytoDelever.svg';
import delevered from './delevered.svg';

const svgIcons = {
  dashboardIcon,
  adminIcon,
  creations,
  transactions,
  reports,
  utilities,
  areachart,
  barCHart,
  pieGold,
  pieSilver,
  pieDiamonds,
  piePlatinum,
  pieGemStones,
  registered,
  readytoDelever,
  delevered
};

export default svgIcons;
