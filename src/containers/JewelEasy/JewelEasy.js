import React, { Component } from 'react';

import Aux from '../../hoc/auxillary';
import svgIcons from '../../assets/images/images';
import SideDrawer from '../../components/Navigation/SideDrawer/SideDrawer';
import MainContainer from '../../components/MainContainer/MainContainer';
// const DECLARE_CONSTANT = {};

class JewelEasy extends Component {
  render() {
    return (
      <Aux>
        <div className="main-wrapper">
          <SideDrawer />
          <MainContainer />
        </div>
      </Aux>
    );
  }
}
export default JewelEasy;
